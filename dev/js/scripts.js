var app = app || {};

app.init = function() {

  app.date();
  app.slider();
  app.navMobile();
  // app.video();
  app.animation();
  app.fancybox();
  app.fancyBoxVideo();
};

app.slider = function() {
  $('.main-visual-in').slick({
    dots: true,
    infinite: true,
    speed: 500,
    autoplay: false,
    autoplaySpeed: 2000,
  });
};

app.date = function() {
  $("#datepicker").datepicker({
    numberOfMonths: [1, 1],
    dateFormat: "dd/mm/yy",
  });

  $("#datepicker02").datepicker({
    numberOfMonths: [1, 1],
    dateFormat: "dd/mm/yy"
  });

  $('.item-news .desc.date-time').click(function() {
    $(this).parent().addClass('active');
    return false;
  });
};
/* navigation for mobile */
var offsetY = window.pageYOffset;
app.navMobile = function() {
  $('.hambuger-menu').click(function(event) {
    event.preventDefault();
    $(this).toggleClass('is-down');
    var $nav = $(this).parent().find('.navigation');

    if ($(this).hasClass('is-down')) {
      offsetY = window.pageYOffset;
      $('body').css({
        'overflow-y': 'hidden',
        'top': -offsetY + 'px'
      });
      app.navFit();
      $nav.stop().slideDown(function() {
        app.navFit();
      });

    } else {
      $nav.stop().slideUp();
      $('body').css('overflow-y', 'scroll');
      $(window).scrollTop(offsetY);
    }

    if (!$(this).hasClass('is-down')) {
      $(this).addClass('is-out');
      setTimeout(function() {
        $('.hambuger-menu').removeClass('is-out');
      }, 500);

    }

    return false;

  });
};

app.navFit = function() {
  function navHeight() {
    var navi = $(".header-inner");
    var naviInner = $(".header-inner .navigation");
    if (navi.height() >= window.innerHeight) {
      naviInner.css({
        "height": window.innerHeight - 60 + "px"
      });
    } else {
      naviInner.css({
        "height": "auto"
      });
    }
  }
  navHeight();
  $(window).on("load resize", function() {
    navHeight();
  });
}


function myMap() {
  var myCenter = new google.maps.LatLng(10.2222167, 105.7879371);
  var map = new google.maps.Map(document.getElementById('maps'), {
    zoom: 14,
    center: myCenter
  });
  var marker = new google.maps.Marker({
    position: myCenter,
    icon: 'img/common/icon_maps.png',
    map: map

  });

}

app.animation = function() {
  var wow = new WOW({
    boxClass: 'wow',
    animateClass: 'animated',
    offset: 0,
    mobile: false,
    live: true
  });
  wow.init();
};


app.fancybox = function() {
  $('.fancybox').fancybox();
};


app.fancyBoxVideo = function () {
  $('[data-fancybox]').fancybox({
    toolbar: false,
    afterLoad : function( instance, current ) {
      current.$content.append('<div class="btn-close" data-fancybox-close></div>');
      current.$content.append('<div class="theater-overlay"><div class="ov-before"></div><div class="ov-after"></div></div>');
      setTimeout(function() {
        $('.theater-overlay .ov-before').addClass('full');
      }, 500);
      setTimeout(function() {
        $('.theater-overlay .ov-after').addClass('full');
      }, 2000);
      setTimeout(function() {
        var iframeSrc = current.$content.find('iframe').attr('src');
        // iframeSrc = iframeSrc.replace('autoplay=0', 'autoplay=1');
        current.$content.find('iframe').attr('src', iframeSrc);
      }, 3000);
      setTimeout(function() {
        current.$content.find('iframe').css('opacity', 1);
        current.$content.find('.btn-close').css('opacity', 1);
      }, 4000);

      current.$content.css({
        overflow   : 'visible',
        background : '#000'
      });
    }
  });
}



$(function() {

  app.init();

});
